# Atlassian Connect Core (for Haskell)

This is a library that provides Atlassian Connect functionality for applications built on top of the
Snap Framework. We provide a snaplet so that you can integrate Atlassian Connect into your
applications.

{-|
Module      : Snap.AtlassianConnect
Description : Defines a snaplet for the Atlassian Connect framework.
Copyright   : (c) Robert Massioli, 2014
License     : APACHE-2
Maintainer  : rmassaioli@atlassian.com
Stability   : experimental

This module provides an Snap Framework Snaplet for the Atlassian Connect framework. In order to use it in your
snap application you just need to run initConnectSnaplet in your initialiser like so:

> appConnect <- nestSnaplet "connect" connect (initConnectSnaplet myPluginDescriptor)

All you need to provide us with is a descriptor that you have generated using the
<http://hackage.haskell.org/package/atlassian-connect-descriptor atlassian-connect-descriptor library>.

It is important to note that certain configuration values can be overriden with Environment Variables because they may
be specific to the environment that you deployed the service to:

* Use CONNECT_BASE_URL to override the Base Url in the Addon Descriptor.
* Use CONNECT_SECRET_KEY to override the Page Token Secret Key.

To test that it has worked you can start your application and find your descriptor at: \/connect\/atlassian-connect.json

We will also automatically create a connect.cfg file for you in the snaplets/connect directory. If this does not happen
then delete the snaplets/connect directory and restart your application. Please have a look inside the connect.cfg directory
as there will be plugin specific setting that you will have to override. It is likely that you will want to override all
of those configuration options.
-}
module Snap.AtlassianConnect (
    -- * Connect Snaplet
    initConnectSnaplet
    , Connect(..)
    , HasConnect(..)
    , HostName
    -- * Atlassian Standard Types
    , UserKey
    , UserEmail
    , IssueKey
    , IssueId
    , ProjectKey
    , ProjectId
    , IssueSummary
    , UserDetails(..)
    , IssueDetails(..)
    -- * Lifecycle and Tenant
    , getLifecycleResponse
    , LifecycleResponse(..)
    , ClientKey
    , Tenant(..)
    , TenantKey
    , TenantWithUser
    -- * Page Tokens
    , PageToken(..) -- TODO make it so that you can query the token but nothing else
    , generateToken
    , generateTokenCurrentTime
    , encryptPageToken
    , decryptPageToken
    , defaultTimeoutSeconds
    -- * Routes Helpers
    , homeHandler
    -- * Connect Data Types
    , ConnectURI(..)
    , getURI
    , ConnectSecond(..)
    , getSecond
    ) where

import           Snap.AtlassianConnect.AtlassianTypes
import           Snap.AtlassianConnect.Connect
import           Snap.AtlassianConnect.Data
import           Snap.AtlassianConnect.Instances
import           Snap.AtlassianConnect.LifecycleResponse
import           Snap.AtlassianConnect.PageToken
import           Snap.AtlassianConnect.Routes
import           Snap.AtlassianConnect.Tenant
import           Snap.AtlassianConnect.TimeUnits

